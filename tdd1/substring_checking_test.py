import unittest
from substring_checking import substring_checker
from read_file import Read
from unittest import mock

w=Read("input.txt").readFromFile()

class beforemethod(unittest.TestCase):
    #this setup will behave as @before in java
    def setUp(self):
        self.word=substring_checker()
    # this teardown will behave as @after in java
    def tearDown(self) :
        self.word=None


class Test_substring(beforemethod):

    #mocking a class
    @mock.patch("read_file.Read")
    def test_mock(self,mock_b_constructor):
        mock_b=mock_b_constructor.return_value
        mock_b.readFromFile.return_value=1
        words=w[2].split(",")
        self.assertEqual(self.word.isSubstring(words[0].strip(),words[1].strip()),1)
    def test_null_string(self):
        words=w[0].split(",")
        # substring_checker1=substring_checker()
        self.assertEqual(self.word.isSubstring(words[0].strip(),words[1].strip()),-1)

    def test_first_string(self):
        words=w[1].split(",")

        self.assertEqual(self.word.isSubstring(words[0].strip(),words[1].strip()),1)
    def test_third_string(self):
        words=w[3].split(",")
        self.assertEqual(self.word.isSubstring(words[0].strip(),words[1].strip()),1)

    def test_not_present_string(self):
        words=w[5].split(",")
        self.assertEqual(self.word.isSubstring(words[0].strip(),words[1].strip()),-1)
