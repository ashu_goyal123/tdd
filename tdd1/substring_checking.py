from dataclasses import dataclass

class substring_checker(object):

  def isSubstring(self,s1, s2):
    M = len(s1)
    N = len(s2)

    # A loop to slide pat[] one by one
    for i in range(N - M + 1):

        # For current index i,
        # check for pattern match
        for j in range(M):
            if (s2[i + j] != s1[j]):
                break
            if j + 1 == M:
                return 1

    return -1


s=substring_checker()

